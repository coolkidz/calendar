/**
 * Created by kquinn on 4/11/16.
 */

Number.prototype.padLeft = function(base,chr){
    var  len = (String(base || 10).length - String(this).length)+1;
    return len > 0? new Array(len).join(chr || '0')+this : this;
};

$(function() { //at document ready
    var ll = new LiveLinks(); //for authentication purposes
    var firebase = new Firebase("https://victoriajohnston.firebaseio.com/");
    var homepage = true;

    function editEvent(event){ //allows you to edit an event
        //a lot of this code is editing the dialog such that the original event data is propogated onto the dialog for the event
        console.log(event);
        var startdate = new Date(event.start._d); // for specific date formatting - fullcalendar only allows specific date format
        startdate = [(startdate.getDate()).padLeft(),
                (startdate.getMonth()+1).padLeft(),
                startdate.getFullYear()].join('/') +' ' +
            [startdate.getHours().padLeft(),
                startdate.getMinutes().padLeft(),
                startdate.getSeconds().padLeft()].join(':');
        var enddate;
        if (event.end !== null){
            enddate = new Date(event.end._d);
            enddate = [enddate.getDate().padLeft(),
                    (enddate.getMonth()+1).padLeft(),
                    enddate.getFullYear()].join('/') +' ' +
                [enddate.getHours().padLeft(),
                    enddate.getMinutes().padLeft(),
                    enddate.getSeconds().padLeft()].join(':');
        }
        console.log(startdate);
        $('#editstart').datetimepicker({ //datepicker GUI for start date
            format: 'dd/MM/yyyy hh:mm:ss'
        });
        $('#editstartdate').val(startdate);
        $('#editend').datetimepicker({ //datepicker GUI for end date
            format: 'dd/MM/yyyy hh:mm:ss'
        });
        $('#editenddate').val(enddate);
        // check if private
        var uid = ll.uid(); //get the current user's id on firebase
        var req = null;
        var publicRef = new Firebase(firebase + "members/"+uid+"/events/"+event.constraint+"/");
        //we check firebase to see if the event is public or private and set the edit radio button appropriately
        publicRef.once('value',function(snapshot){
           if (snapshot.child("private").exists()){//private only exists if private or edited
               console.log(snapshot.val().private);
               if (snapshot.val().private === true){
                   // set radio button to private
                   $('#privateradio').prop("checked",true);
                   $('#publicradio').prop("checked",false);
               } else {
                   // set radio button to public
                   $('#privateradio').prop("checked",false);
                   $('#publicradio').prop("checked",true);
               }
           } else { //if private does not exist, it is public
               console.log(snapshot.val());
               // set radio button to public
               $('#privateradio').prop("checked",false);
               $('#publicradio').prop("checked",true);
           }
            // check requests
            if (snapshot.child('req').exists()){
                req = snapshot.val().req;
            }
        });
        $('#editdescription').val(event.description);//set description to intial description
        $("#editEvent").dialog({ //the actual dialog
            buttons: [
                {
                    text: "Save", //this will propogate changes in the calendar and firebase
                    icons: {
                        primary: "ui-icon-heart"
                    },
                    click: function () {
                        //if you click save the entire event should be removed from fullcalendar and then added again (assuming delete event is not checked)
                        $('#calendar').fullCalendar('removeEvents',event._id);
                        var uid = ll.uid();
                        var eventsRef = new Firebase("https://victoriajohnston.firebaseio.com/members/"+uid+"/events/"+event.constraint+"/");

                        if (document.getElementById("deleteevent").checked){
                            //if the delete event checkbox is checked, do not add the event to fullcalendar(the webpage) and remove the event on firebase
                            $('#deleteevent').prop("checked",false);
                            if (req !== null) {
                                var otherRef = new Firebase("https://victoriajohnston.firebaseio.com/members/"+req+"/events/"+event.constraint+"/");
                                otherRef.remove(); //remove on firebase from both users if it was a request
                            }
                            eventsRef.remove();//remove on firebase from single user
                        } else {
                            //this means you should not delete the event but edit the data
                            //therefore it will be readded to the fullcalendar data
                            var eventtitle = event.title;

                            // get changed data
                            var startdate = $(this).find('#editstartdate').val();
                            console.log(startdate);
                            var enddate = $(this).find('#editenddate').val();
                            console.log(enddate)
                            var startstr;
                            if (startdate) {
                                //format the string correctly
                                var sdarray = startdate.split(/[\s/:]+/);
                                startstr = sdarray[2] + "-" + sdarray[1] + "-" + sdarray[0] + "T" + sdarray[3] + ":" + sdarray[4] + ":" + sdarray[5]+"-04:00";
                            } else {
                                startstr = event.start._d;
                            }
                            var endstr = null;
                            if (enddate){ //format the string correctly if enddate
                                //an enddate does not necessarily need to be there
                                var edarray = enddate.split(/[\s/:]+/);
                                endstr = edarray[2]+"-"+edarray[1]+"-"+edarray[0]+"T"+edarray[3]+":"+edarray[4]+":"+edarray[5]+"-04:00";
                            } else {
                                endstr = event.end._d;
                            }

                            //this edits the description to either the old or new value if edited
                            var description = $(this).find('#editdescription').val();
                            if (!description) {
                                description = event.description;
                            }

                            var constraint = event.constraint;
                            var newEvent = { //the event data for fullcalendar
                                title: eventtitle,
                                start: new Date(startstr),
                                end: new Date(endstr),
                                description: description,
                                constraint: constraint
                            };
                            console.log(newEvent);

                            //add the event to full calendar
                            $('#calendar').fullCalendar('renderEvent', newEvent, 'stick'); //this creates the event

                            // get event from firebase
                            var private = ($('input[name=access]:checked', '#editaccesscontrol').val() === "true");
                            newEvent["private"] = private;
                            newEvent['req'] = req;
                            eventsRef.update(newEvent);
                            // change event for the other user if it is a request
                            if (req !== null){
                                var otherRef = new Firebase("https://victoriajohnston.firebaseio.com/members/"+req+"/events/"+event.constraint+"/");
                                otherRef.once('value',function(snapshot){
                                    newEvent['title'] = snapshot.val().title;
                                    newEvent['req'] = ll.uid();
                                    otherRef.update(newEvent);
                                });
                            }
                        }

                        $(this).dialog("close"); //close the dialog and blur/remove the values on the dialog to prepare for initialization again
                        $('#editstartdate').val("").blur();
                        $('#editenddate').val("").blur();
                        $('#editdescription').val("").blur();
                        return false;
                    }
                },
                {
                    text: "Cancel",//do not do anything to the data
                    icons: {
                        primary: "ui-icon-trash"
                    },
                    click: function () {
                        $(this).dialog("close"); //close the dialog and blur values
                        $('#editstartdate').val("").blur();
                        $('#editenddate').val("").blur();
                        $('#editdescription').val("").blur();
                        $('#deleteevent').prop("checked",false);
                        return false;
                    }
                }
            ],
            closeOnEscape: false,
            //this removes the 'x' on the upper right corner, we want the user to click on an actual button
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
        });
    }

    var $loginButton = $('#login-button'),
        $signupButton = $('#signup-button'),
        $logoutButton = $('#logout-button'),
        $loginForm = $('#login-form'),
        $facebookButton = $('#facebook-button'),
        $signupForm = $('#signup-form'),
        $calendar = $('#calendar'),
        $addEvent = $('#addEvent'),
        $reqEvent = $('#request'),
        $changeView = $('#changeView'),
        $loginTitle = $('#logintitle'),
        $backButton = $('#back-button'),
        $backButton2 = $('#back-button2'),
        $panel = $('#panel'),
        $deleteGroup = $('#deletegroup'),
        $alerts;

    ll.onLogin = function(user) { //login function
        //  console.log(user);
        console.log("in onLogin!");
        //display and hide the appropriate buttons needed
        $loginButton.hide();
        $signupButton.hide();
        $facebookButton.hide();
        $logoutButton.show();
        $signupForm.hide();
        $loginForm.hide();
        $calendar.show();
        $addEvent.show();
        $reqEvent.hide();
        $panel.show();
        $loginTitle.hide();
        $changeView.show();
        homepage = true;

        $('#calendar').fullCalendar({ //this initializes the full calendar
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            selectable: true,
            selectHelper: true,
            editable: false,//do not allow the event to be dragged, user must go through the edit dialog to actually edit the event
            draggable: false,
            eventClick: function (event, jsEvent, view) {
                //set the values and open the modal to display event data
                console.log(event);
                var start_formatted = event.start._d.toString();
                start_formatted = start_formatted.replace("GMT-0400 (EDT)",""); //this is done to correct the timezone
                $('#startTime').html("Start: "+start_formatted);
                if (event.end !== null){
                    var end_formatted = event.end._d.toString();
                    end_formatted = end_formatted.replace("GMT-0400 (EDT)","");
                    $('#endTime').html("End: "+end_formatted);
                }
                if (event.description !== null){
                    $("#eventInfo").html(event.description);
                }
                if (homepage){
                    //if you are at your home calendar you are allowed to edit your events, otherwise you cannot edit
                    $("#eventContent").dialog({
                        buttons: [
                            {
                                text: "Edit",
                                icons: {
                                    primary: "ui-icon-heart"
                                },
                                click: function () {
                                    $(this).dialog("close");
                                    editEvent(event);
                                    return false;
                                }
                            },
                            {
                                text: "Close",
                                icons: {
                                    primary: "ui-icon-trash"
                                },
                                click: function () {
                                    $(this).dialog("close");
                                    return false;
                                }
                            }
                        ],
                        // modal: true,
                        title: event.title,
                        closeOnEscape: false,
                        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
                    });
                } else {
                    //if not your calendar or your events on the group page, you cannot edit the events, must be on personal calendar to do so
                    $("#eventContent").dialog({
                        buttons: [
                            {
                                text: "Close",
                                icons: {
                                    primary: "ui-icon-trash"
                                },
                                click: function () {
                                    $(this).dialog("close");
                                    return false;
                                }
                            }
                        ],
                        title: event.title,
                        closeOnEscape: false,
                        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
                    });
                }

                return false;
            },
            ignoreTimezone: false
        });

        console.log("data",$('#calendar_data').data());
        var eventarray = $('#calendar_data').data();
        for(var i in eventarray){//stick all events for the user on the calendar
            if (eventarray.hasOwnProperty(i)){
                $('#calendar').fullCalendar('renderEvent', eventarray[i], 'stick');
            }
        }

        var membersRef = new Firebase(firebase+"members/"); //this is creating the user list to display all other users beside yourself
        membersRef.once("value",function(snapshot){
           snapshot.forEach(function(childSnapshot){
               if (childSnapshot.key() !== ll.uid()){
                   var childData = childSnapshot.val();
                   var $li = $("<li class='userli'>"+childData.name+"</li>");
                   $li.data({key:childSnapshot.key()});
                   $('#userlist').append($li);
               }
           });
        });

        var uid = ll.uid();
        //edit the group list for all of your groups that you belong to
        var groupsRef = new Firebase(membersRef+"/"+uid+"/groups/");
        groupsRef.once("value",function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.key();
                var $li = $("<li class='groupli'>"+childData+"</li>"); //the group li will have the group member data needed
                var memberlist = [];
                childSnapshot.forEach(function(groupmembers){
                    memberlist.push(groupmembers.key());
                });
                $li.data({members:memberlist});
                console.log($li);
                $('#grouplist').append($li);
            });
        });

        //add the requests on the request list
        var reqRef = new Firebase(membersRef+"/"+uid+"/requests/");
        reqRef.once("value",function(snapshot){
            console.log(snapshot);
            snapshot.forEach(function(childSnapshot){
                console.log(childSnapshot);
                var eventtitle = childSnapshot.val().title;
                var $li = $("<li class='reqli'>"+eventtitle+"</li>");
                $li.data({key:childSnapshot.key()});
                console.log($li);
                $('#requestlist').append($li);
            });
        });
    };

    ll.onLogout = function() {
       // console.log("in onLogout");
        //on logout display/hide the appropriate buttons
        $loginButton.show();
        $signupButton.show();
        $logoutButton.hide();
        $loginForm.hide();
        $signupForm.hide();
        $facebookButton.show();
        $calendar.hide();
        $addEvent.hide();
        $reqEvent.hide();
        $panel.hide();
        $deleteGroup.hide();
        $changeView.hide();
        $loginTitle.show();
    };

    ll.onLoginFailure = function() {
        console.log("in onLoginFailure");
        $loginButton.show();
        $signupButton.show();
        //$backButton.hide();
    };

    $logoutButton.on('click',function(e) {
        //ask the user if they are sure that they want to logout
        var logout_popup = $('<div id="dialog">Are you sure you want to logout?</div>');
        logout_popup.dialog({
            buttons: [
                {
                    text: "Yes",
                    icons: {
                        primary: "ui-icon-heart"
                    },
                    click: function() { //if yes, logout, hide/show buttons, and empty lists appropriately
                        ll.logout();
                        $logoutButton.hide();
                        $loginButton.show();
                        $signupButton.show();
                        $('#userlist').empty();
                        $('#grouplist').empty();
                        $('#groupmembers').empty();
                        document.getElementById("groupmemberstitle").style.display = "none";
                        $('#calendar').fullCalendar('destroy');
                        $('#calendar_data').removeData();
                        $( this ).dialog( "close" );
                        return false;
                    }
                },
                {
                    text: "No thanks", //else stay logged in
                    icons: {
                        primary: "ui-icon-trash"
                    },
                    click: function() {
                        $( this ).dialog( "close" );
                        return false;
                    }
                }
            ]
        });
        $(".ui-dialog-titlebar").hide();
        $(".ui-dialog-content").css("background-color", "#FFF");
    });

    ll.onError = function(error) {
        //on error show the appropriate error and buttons
        showAlert(error,"danger");
        $loginButton.show();
        $signupButton.show();
        $logoutButton.hide();
        $loginForm.hide();
        $signupForm.hide();

    };

    //display/hide the appropriate buttons
    $loginButton.show();
    $signupButton.show();
    $logoutButton.hide();
    $loginForm.hide();
    $signupForm.hide();
    $calendar.hide();
    $addEvent.hide();
    $reqEvent.hide();
    $changeView.show();
    $loginTitle.show();
    $panel.hide();
    $deleteGroup.hide();


    $loginButton.on('click',function(e) {
        //on click hide/show
        //console.log("clicked login button!");
        $loginButton.hide();
       // $facebookButton.hide();
        $signupButton.hide();
        $signupForm.hide();
        $loginForm.show();
        $('#login-email').val("").focus();
        $('#login-password').val("").blur();
        return false;
    });

    $loginForm.on('submit',function(e) {
        //login form submit click
        $loginForm.hide();
        e.preventDefault();
        e.stopPropagation();
        ll.login($(this).find('#login-email').val(), $(this).find('#login-password').val());
        $('#login-email').val("").blur();
        $('#login-password').val("").blur();
        return false;
    });

    $signupButton.on('click',function(e) {
        //sign up button hide/show button
        //console.log("clicked signup button");
        $signupButton.hide();
        $loginButton.hide();
        $loginForm.hide();
        $signupForm.show();
        $facebookButton.hide();
        $('#signup-email').val("").focus();
        $('#signup-name').val("").blur();
        $('#signup-phone').val("").blur();
        $('#signup-street').val("").blur();
        $('#signup-city').val("").blur();
        $('#signup-state').val("").blur();
        $('#signup-zip').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-verify-password').val("").blur();
        return false;
    });

    $backButton.on('click',function(e){
        //on back button show appropriate buttons
        $signupForm.hide();
        $loginForm.hide();
        $loginButton.show();
        //$facebookButton.show();
        $signupButton.show();
        return false;
    });

    $backButton2.on('click',function(e){
        $signupForm.hide();
        $loginForm.hide();
        $loginButton.show();
        //$facebookButton.show();
        $signupButton.show();
        return false;
    });

    $signupForm.on('submit', function(e) {
        //sign up form submit
        $signupForm.hide();
        e.preventDefault();
        e.stopPropagation();
        if ($(this).find('#signup-password').val()===$(this).find('#signup-verify-password').val()){
            ll.signup($(this).find('#signup-email').val(),
                $(this).find('#signup-name').val(),
                $(this).find('#signup-phone').val(),
                $(this).find('#signup-street').val(),
                $(this).find('#signup-city').val(),
                $(this).find('#signup-state').val(),
                $(this).find('#signup-zip').val(),
                $(this).find('#signup-password').val());
        } else {
            alert("Passwords do not match");
            $loginButton.show();
            $facebookButton.show();
            $signupButton.show();
        }
        $('#signup-email').val("").focus();
        $('#signup-name').val("").blur();
        $('#signup-phone').val("").blur();
        $('#signup-street').val("").blur();
        $('#signup-city').val("").blur();
        $('#signup-state').val("").blur();
        $('#signup-zip').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-verify-password').val("").blur();
    });

    function showAlert(message, type) {
        //show alert function
        var $alert = (
            $('<div>')                // create a <div> element
                .text(message)          // set its text
                .addClass('alert')      // add some CSS classes and attributes
                .addClass('alert-' + type)
                .addClass('alert-dismissible')
                .hide()  // initially hide the alert so it will slide into view
        );

        /* Add the alert to the alert container. */
        $alerts = $('#alerts'); //alerts is not defined fix this
        $alerts.append($alert);

        $alerts.on('click',function(e) {
            var $t=$(e.target);
            $t.remove();
        });

        /* Slide the alert into view with an animation. */
        $alert.slideDown();
        setTimeout(function(){$alert.hide()},3000);
    }

    ll.logout();
    ll.start();

    $addEvent.on("click", function (e) {
        $('#startdatepicker').datetimepicker({ //datepicker GUI for add event start
            format: 'dd/MM/yyyy hh:mm:ss'
        });
        $('#enddatepicker').datetimepicker({
            format: 'dd/MM/yyyy hh:mm:ss'
        });
        $('#datepicker').dialog({
            buttons: [
                {
                    text: "Yes", //add the event on firebase and full calendar
                    icons: {
                        primary: "ui-icon-heart"
                    },
                    click: function () {
                        var eventtitle = $(this).find('#eventtitle').val();
                        var startdate = $(this).find('#startdate').val();
                        var enddate = $(this).find('#enddate').val();
                        var sdarray = startdate.split(/[\s/:]+/);
                        var startstr = sdarray[2]+"-"+sdarray[1]+"-"+sdarray[0]+"T"+sdarray[3]+":"+sdarray[4]+":"+sdarray[5]+"-04:00";
                        var endstr = null;
                        if (enddate){
                            var edarray = enddate.split(/[\s/:]+/);
                            endstr = edarray[2]+"-"+edarray[1]+"-"+edarray[0]+"T"+edarray[3]+":"+edarray[4]+":"+edarray[5]+"-04:00";
                        }

                        var private = ($('input[name=access]:checked', '#accesscontrol').val() === "true");

                        var description = $(this).find('#description').val();

                        var eid = createFirebaseEvent(eventtitle, startstr, endstr,description, private);
                        console.log(eid);
                        var newEvent = {
                            title: eventtitle,
                            start: new Date(startstr),
                            end: new Date(endstr),
                            description: description,
                            constraint: eid
                        };

                        $('#calendar').fullCalendar('renderEvent', newEvent, 'stick'); //this creates the event
                        $(this).dialog("close");
                        $('#eventtitle').val("").blur();
                        $('#startdate').val("").blur();
                        $('#enddate').val("").blur();
                        $('#description').val("").blur();
                        return false;
                    }
                },
                {
                    text: "No thanks", //else do nothing
                    icons: {
                        primary: "ui-icon-trash"
                    },
                    click: function () {
                        $(this).dialog("close");
                        $('#eventtitle').val("").blur();
                        $('#startdate').val("").blur();
                        $('#enddate').val("").blur();
                        $('#description').val("").blur();
                        return false;
                    }
                }
            ],
            closeOnEscape: false,
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();  }
        });
    });

    $reqEvent.on('click',function(e){
        $('#startdatepicker').datetimepicker({
            format: 'dd/MM/yyyy hh:mm:ss'
        });
        $('#enddatepicker').datetimepicker({
            format: 'dd/MM/yyyy hh:mm:ss'
        });
        $('#datepicker').dialog({
            buttons: [
                {
                    text: "Yes", //request the event and put on firebase for the other user to see on their next login
                    icons: {
                        primary: "ui-icon-heart"
                    },
                    click: function () {
                        var eventtitle = $(this).find('#eventtitle').val();
                        var startdate = $(this).find('#startdate').val();
                        var enddate = $(this).find('#enddate').val();
                        var sdarray = startdate.split(/[\s/:]+/);
                        var startstr = sdarray[2]+"-"+sdarray[1]+"-"+sdarray[0]+"T"+sdarray[3]+":"+sdarray[4]+":"+sdarray[5]+"-04:00";
                        var endstr = null;
                        if (enddate){
                            var edarray = enddate.split(/[\s/:]+/);
                            endstr = edarray[2]+"-"+edarray[1]+"-"+edarray[0]+"T"+edarray[3]+":"+edarray[4]+":"+edarray[5]+"-04:00";
                        }

                        var private = ($('input[name=access]:checked', '#accesscontrol').val() === "true");

                        var description = $(this).find('#description').val();

                        var user = $reqEvent.data()['user'];
                        var eventsRef = new Firebase("https://victoriajohnston.firebaseio.com/members/"+user+"/requests/");
                        var newevent = {};
                        newevent["title"] = eventtitle;
                        newevent["start"] = startstr;
                        newevent["end"] = endstr;
                        newevent["description"] = description;
                        newevent["private"] = private;
                        newevent["requestfrom"] = ll.uid();
                        eventsRef.push(newevent);


                      //  $('#calendar').fullCalendar('renderEvent', newEvent, 'stick'); //this creates the event
                        $(this).dialog("close");
                        $('#eventtitle').val("").blur();
                        $('#startdate').val("").blur();
                        $('#enddate').val("").blur();
                        $('#description').val("").blur();
                        return false;
                    }
                },
                {
                    text: "No thanks", //do not request the event
                    icons: {
                        primary: "ui-icon-trash"
                    },
                    click: function () {
                        $(this).dialog("close");
                        $('#eventtitle').val("").blur();
                        $('#startdate').val("").blur();
                        $('#enddate').val("").blur();
                        $('#description').val("").blur();
                        return false;
                    }
                }
            ],
            closeOnEscape: false,
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();  }
        });
    });

    function createFirebaseEvent(title, start, end, description, private, reqfrom){
        //this creates a firebase event for the user, storing the appropriate data
        reqfrom = reqfrom || null;
        var uid = ll.uid();
        var eventsRef = new Firebase("https://victoriajohnston.firebaseio.com/members/"+uid+"/events/");
        var newevent = {};
        newevent["title"] = title;
        newevent["start"] = start;
        newevent["end"] = end;
        newevent["description"] = description;
        newevent["private"] = private;
        if (reqfrom !== null){
            newevent["req"] = reqfrom;
        }
        var newEventRef = eventsRef.push(newevent);
        return newEventRef.key();
    }

    $('#userlist').on("click",'li',function(event){
        $('#groupmembers').empty(); //on click hide things
        document.getElementById("groupmemberstitle").style.display = "none";
        var userid = $(this).data()["key"];
        refreshEvents(userid); //refresh the calendar events for the appropriate user id stored in the list element
        $addEvent.hide();
        $reqEvent.show();
        $deleteGroup.hide();
        $reqEvent.data({user:userid});
        homepage = false;
    });

    $('#home').on("click",function(event){
        //on click show add event and hide appropriate buttons
        $('#groupmembers').empty();
        document.getElementById("groupmemberstitle").style.display = "none";
        refreshEvents(ll.uid());//refresh the events for yourself
        $addEvent.show();
        $reqEvent.hide();
        $deleteGroup.hide();
        homepage = true; //set the variable to true so certain conditions happen
    });


    function refreshEvents(userid){
        // remove all events from the calendar
        $('#calendar').fullCalendar('removeEvents');
        var userRef = new Firebase(firebase+'members/'+userid+'/events/'); //for user id passed in to the funciton
        userRef.once("value",function(snapshot) {
            console.log(snapshot.val());
            snapshot.forEach(function (childshapshot) {
                var eventdata = childshapshot.val();
                if ((!homepage && !eventdata["private"]) || homepage){
                    //if not homepage and public add to the events or if homepage add all events
                    var eid = childshapshot.key();
                    var newEvent = {
                        title: eventdata["title"],
                        start: new Date(eventdata["start"]),
                        end: new Date(eventdata["end"]),
                        //editable: true,
                        description: eventdata["description"],
                        constraint: eid
                    };
                    $('#calendar').fullCalendar('renderEvent', newEvent, 'stick');
                }
            });
        });
    }

    $('#grouplist').on("click",'li',function(event){
        //on group list click show appropriate events
        $('#groupmembers').empty(); //empty to not repeat add the members
        $deleteGroup.show();
        var groupobj = {'groupname': $(this).text() }; //store the groupname
        groupobj["members"] = $(this).data()["members"];
        groupobj["this"] = this;
        $deleteGroup.data(groupobj)
        $addEvent.hide();
        $reqEvent.hide();
        document.getElementById("groupmemberstitle").style.display = "block";
        homepage = false;
        $('#calendar').fullCalendar('removeEvents');//remove all events
        var members = $(this).data()["members"];
        console.log(members);
        var colours = ["red","green","blue","cyan", "orange","pink","purple","teal","yellow"];
        var i = 0;
        for (var j=0; j<members.length; j++){
            var member = members[j];
            // get members name
            //var userRef = new Firebase(firebase+'members/'+member+'/events/');
            var userRef = new Firebase(firebase+'members/'+member+'/');
            userRef.child('name').once('value',function(snapshot){
                var $li = $("<li class='groupmembersli'>"+snapshot.val()+"</li>");
                $('#groupmembers').append($li);
            });
            console.log(userRef);
            userRef.child('events').once("value",function(snapshot) {
                console.log(snapshot.val());
                if (i >= 9){
                    i=0;
                }
                console.log("i",i);
                var colour = colours[i];
                i++;
                $('#groupmembers li:last-child').css('color',colour);
                console.log(colour);
                snapshot.forEach(function (childshapshot) {
                    var eventdata = childshapshot.val();
                    if (!eventdata["private"]){
                        var eid = childshapshot.key();
                        var newEvent = {
                            title: eventdata["title"],
                            start: new Date(eventdata["start"]),
                            end: new Date(eventdata["end"]),
                            description: eventdata["description"],
                            constraint: eid,
                            color: colour
                        };
                        $('#calendar').fullCalendar('renderEvent', newEvent, 'stick'); //add all events
                    }
                });
            });
        }
    });

    $('#requestlist').on("click",'li',function(event){
        //on request list click, decide to add the event to firebase or delete request
        $deleteGroup.hide();
        var reqeid = $(this).data()["key"];
        console.log(reqeid);
        // get request from name
        var reqRef = new Firebase(firebase+"/members/"+ll.uid()+"/requests/"+reqeid+"/");
        reqRef.once('value',function(snapshot){
            var event = snapshot.val();
            var reqfromRef = new Firebase(firebase+"/members/"+event.requestfrom+"/");
            var reqfromname;
            reqfromRef.once('value',function(childsnap){
                reqfromname = childsnap.val().name;
                $('#reqfrom').html("Request from: "+childsnap.val().name);
            });

            $('#reqstart').html(event.start);
            $('#reqend').html(event.end);
            $('#reqevent').html(event.description);
            $("#requestDialog").dialog({
                buttons: [
                    {
                        text: "Yes", //if yes add the event to firebase and yourself
                        icons: {
                            primary: "ui-icon-heart"
                        },
                        click: function () {
                            // add event to firebase for both people
                            console.log(event);
                            var requid = event['requestfrom'];
                            delete event['requestfrom'];
                            var oldtitle = event.title;
                            var newtitle = oldtitle + " with " + reqfromname;
                            event['title'] = newtitle;
                            var eid = createFirebaseEvent(newtitle, event.start, event.end, event.description, event.private, requid);
                            event['constraint'] = eid;
                            $('#calendar').fullCalendar('renderEvent', event, 'stick');

                            // add event to request from persons calendar
                            var myRef =  new Firebase(firebase+"/members/"+ll.uid()+"/");
                            myRef.once('value',function(childsnap){
                                var name = childsnap.val().name;
                                var reqfromRef = new Firebase("https://victoriajohnston.firebaseio.com/members/"+requid+"/events/");
                                var newtitle = oldtitle + " with " + name;
                                event['title'] = newtitle;
                                event['req'] = ll.uid();
                                var sameevent = {};
                                sameevent[eid] = event;
                                reqfromRef.update(sameevent);
                            });
                            $(this).dialog("close");
                            return false;
                        }
                    },
                    {
                        text: "No", //else do nothing
                        icons: {
                            primary: "ui-icon-trash"
                        },
                        click: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    }
                ],
                // modal: true,
                title: event.title,
                closeOnEscape: false,
                open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
            });

            // clear event from list
            // clear the dialog
            $('#reqstart').val("").blur();
            $('#reqend').val("").blur();
            $('#reqevent').val("").blur();
            $('#reqfrom').val("").blur();
            // remove request from firebase
            reqRef.remove();
        });
        event.target.remove();
    });

    $('#deletegroup').on('click',function(event){
        //on click delete the group
        var groupname = $(this).data()["groupname"];
        var members = $(this).data()["members"];
        for (var j=0; j<members.length; j++) {
            var member = members[j];
            var userGroupRef = new Firebase(firebase + 'members/' + member + '/groups/' + groupname + '/');
            userGroupRef.remove();
        }
        // remove the list item
        var li = $(this).data()["this"];
        $(li).remove();
        $('#groupmembers').empty();
        $deleteGroup.hide();
        $('#groupmemberstitle').hide();
        $( "#home" ).trigger( "click" );
    });


    $('#creategroup').on('click',function(event){
        // go through user list and make dialog with all the user groups
        $deleteGroup.hide();
        var userlist = {};
        $('#userlist li').each(function (index){
            var $checkbox = $("<input type='checkbox' name='usercheck' value='"+$(this).data()["key"]+"'>"+$(this).text()+"<br>");
            $('#creategroupcheck').append($checkbox);
            userlist[$(this).text()] = $(this).data()["key"];
        });
        console.log(userlist);

        $('#creategroupdialog').dialog({
            //dialog for all users
            buttons: [
                {
                    text: "Yes", //if yes, update firebase and the calendar
                    icons: {
                        primary: "ui-icon-heart"
                    },
                    click: function() {
                        var checkboxes = document.getElementsByName('usercheck');
                        var groupname = $(this).find('#group-name').val();
                        var checkboxesChecked = [];
                        var groupmembers = {};
                        for (var i=0; i<checkboxes.length; i++) {
                            if (checkboxes[i].checked) {
                                groupmembers[checkboxes[i].value] = "";
                                checkboxesChecked.push(checkboxes[i].value);
                            }
                        }
                        groupmembers[ll.uid()] = "";
                        var newgroup = {};
                        newgroup[groupname] = groupmembers;
                        checkboxesChecked.push(ll.uid());

                        for (var j=0; j<checkboxesChecked.length; j++){
                            var user = checkboxesChecked[j];
                            var userRef = new Firebase(firebase+'members/'+user+'/groups/');
                            userRef.update(newgroup);
                        }

                        var $li = $("<li class='groupli'>"+groupname+"</li>");
                        $li.data({members:checkboxesChecked});
                        $('#grouplist').append($li);

                        $( this ).dialog( "close" );
                        $('#group-name').val("").blur();
                        $('input:checkbox').attr('checked',false);
                        $('#creategroupcheck').empty();
                        return false;
                    }
                },
                {
                    text: "No thanks", //don't do anything
                    icons: {
                        primary: "ui-icon-trash"
                    },
                    click: function() {
                        $( this ).dialog( "close" );
                        $('#group-name').val("").blur();
                        $('input:checkbox').attr('checked',false);
                        $('#creategroupcheck').empty();
                        return false;
                    }
                }
            ]
        });
        $(".ui-dialog-titlebar").hide();
        $(".ui-dialog-content").css("background-color", "#FFF");
    });

});


