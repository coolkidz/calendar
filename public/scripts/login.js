/**
 * Created by kquinn on 4/11/16.
 */
//This file will contain the necessary information for logging in

//edited feednd.js file to get this

"use strict";

function LiveLinks() {
    var firebase = new Firebase("https://victoriajohnston.firebaseio.com/");
    //var firebase = new Firebase("https://kquinn-mobileprof.firebaseio.com/");
    this.firebase = firebase;
    var usersRef = this.firebase.child('members');
    this.usersRef = usersRef;
    var uid;
    var linksRef = this.firebase.child('livelinks');
    this.linksRef = linksRef;
    var instance = this;

    this.onLogin = function(user) {};
    this.onLoginFailure = function() {};
    this.onLogout = function() {};
    this.onError = function(error) {};

    this.start = function(){
        firebase.onAuth(function (authResponse){
            if (authResponse) {
                console.log("user is logged in");
                usersRef.child(authResponse.uid).once('value', function (snapshot){
                    instance.user = snapshot.val();
                    instance.onLogin(instance.user);
                });
            } else {
                console.log("User is logged out");
                instance.onLogout();
            }
        });
    };

    this.uid = function() {return uid;};


    this.signup = function(email, name, phone, street, city, state, zip, password){
        this.firebase.createUser({
            email: email,
            password: password
        }, function(error, userData) {
            if (error) {
                instance.onError("Error creating user" + error);
            } else {
                instance.userData = userData;
                console.log("user data",userData);
                var address = {};
                address["city"] = city;
                address["street"] = street;
                address["state"] = state;
                address["zip"] = zip;

                var calendar = {};
                calendar["events"] = "None";

                usersRef.child(userData.uid).set({
                    email: email,
                    name: name,
                    phone: phone,
                    address: address,
                    calendar: calendar
                }, function(error) {
                    if (error) {
                        instance.onError(error);
                    } else {
                        instance.login(email,password);
                    }
                });
            }
        });
    };

    this.login = function(email,password) {
        console.log(firebase);
        this.firebase.authWithPassword({
            email: email,
            password : password
        }, function(error, authData) {
            if (!error) {
                instance.auth = authData;
                console.log("uid:", authData.uid);
                // this.uid = authData.uid;
                var data = {};
                uid = authData.uid;
                console.log(authData.uid);
                firebase.child("members").child(uid).child("events").once("value",function(snapshot){
                    console.log(snapshot.val());
                    var allevents = [];
                    snapshot.forEach(function (childshapshot){
                        var eventdata = childshapshot.val();
                        var eid = childshapshot.key();
                        var newEvent = {
                            title: eventdata["title"],
                            start: new Date(eventdata["start"]),
                            end: new Date(eventdata["end"]),
                            description: eventdata["description"],
                            constraint: eid
                        };
                        allevents.push(newEvent);
                    });
                    console.log(allevents);
                    $('#calendar_data').data(allevents);
                });
            } else {
                instance.onError("login failed! " + error);
                instance.onLoginFailure();
            }
        }, {
            remember : "sessionOnly"
        });
    };
/*
    this.fblogin = function(){
        this.firebase.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
                console.log("Login Failed!", error);
            } else {
                console.log("Authenticated successfully with payload:", authData);
                instance.auth = authData;
                console.log("uid:", authData.uid);
                // this.uid = authData.uid;
                uid = authData.uid;

            }
        });
    };*/

    // logout
    this.logout = function() {
        this.firebase.unauth();
        instance.auth=null;
    };
}
