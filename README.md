# README #

Calendar social media app created for a final project for Web Apps.

### What is this repository for? ###

* Current implementation: a basic calendar app that stores information in Firebase.
* Eventually will act as a social media app that allows certified users to share calendars with their friends


### How do I get set up? ###

* index.html is the main file
* relies on scripts folder - login and main.js files
* Uses the following libraries:
* FullCalendar
* Bootstrap Date Picker